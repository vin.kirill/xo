package com.neomer.xo.ai;

import com.neomer.xo.game.Field;
import com.neomer.xo.common.Pair;
import com.neomer.xo.game.Player;
import com.neomer.xo.ai.strategy.Strategy;

public class AIPlayer implements Player {

    private final char mark;

    private final Strategy strategy;

    public AIPlayer(char mark, Strategy strategy) {
        this.mark = mark;
        this.strategy = strategy;
    }

    @Override
    public Pair<Integer, Integer> play(final Field field) {
        return strategy.resolve(this, field);
    }

    @Override
    public char getMark() {
        return mark;
    }
}
