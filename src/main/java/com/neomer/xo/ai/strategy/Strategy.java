package com.neomer.xo.ai.strategy;

import com.neomer.xo.game.Field;
import com.neomer.xo.common.Pair;
import com.neomer.xo.game.Player;

public interface Strategy {

    Pair<Integer, Integer> resolve(Player owner, final Field field);

}
