package com.neomer.xo.ai.strategy;

import com.neomer.xo.game.Cell;

public class MovingCase {

    private final Cell cell;

    private final int weight;

    public MovingCase(Cell cell, int weight) {
        this.cell = cell;
        this.weight = weight;
    }

    public Cell getCell() {
        return cell;
    }

    public int getWeight() {
        return weight;
    }
}
