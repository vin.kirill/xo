package com.neomer.xo.ai.strategy;

import com.neomer.xo.game.Cell;
import com.neomer.xo.game.Field;
import com.neomer.xo.common.Pair;
import com.neomer.xo.game.Player;

import java.util.ArrayList;
import java.util.List;

public class StrategyImpl implements Strategy {

    private final List<Pair<Integer, Integer>> calculationDirections;

    public StrategyImpl() {
        calculationDirections = new ArrayList<>();
        calculationDirections.add(new Pair<>(1, 0));
        calculationDirections.add(new Pair<>(-1, 0));
        calculationDirections.add(new Pair<>(0, 1));
        calculationDirections.add(new Pair<>(0, -1));
        calculationDirections.add(new Pair<>(1, 1));
        calculationDirections.add(new Pair<>(-1, 1));
        calculationDirections.add(new Pair<>(1, -1));
        calculationDirections.add(new Pair<>(-1, -1));
    }

    @Override
    public Pair<Integer, Integer> resolve(Player owner, Field field) {

        var cases = new ArrayList<MovingCase>();
        for (var it = field.createForEachIterator(); it.hasNext(); ) {
            var cell = it.next();
            if (cell.getOwner() == null) {
                cases.add(new MovingCase(cell, calculateWeight(owner, cell, field)));
            }
        }

        MovingCase bestMove = null;
        for (var move : cases) {
            if (bestMove == null || move.getWeight() > bestMove.getWeight()) {
                bestMove = move;
            }
        }

        return bestMove != null ? new Pair<>(bestMove.getCell().getX(), bestMove.getCell().getY()) : null;
    }

    private int calculateWeight(Player owner, Cell cell, Field field) {
        var max = 0;
        for (var direction : calculationDirections) {
             var result = calculateRow(direction, owner, cell, field);
             if (result > max) {
                 max = result;
             }
        }
        return max;
    }

    private int calculateRow(Pair<Integer, Integer> direction, Player owner, Cell initialCell, Field field) {
        var targetLocation = new Pair<>(
                initialCell.getX() + direction.getFirst(),
                initialCell.getY() + direction.getSecond()
        );
        if (!field.contains(targetLocation.getFirst(), targetLocation.getSecond())) {
            return 0;
        }
        var targetCell = field.getCell(targetLocation.getFirst(), targetLocation.getSecond());
        if (targetCell.getOwner() == null) {
            return 0;
        }
        if (initialCell.getOwner() == null || initialCell.getOwner() == targetCell.getOwner()) {
            return calculateRow(direction, owner, targetCell, field) + 1;
        }
        return 0;
    }
}
