package com.neomer.xo;

import com.neomer.xo.ai.AIPlayer;
import com.neomer.xo.ai.strategy.StrategyImpl;
import com.neomer.xo.game.*;

import java.util.ArrayList;

public class Game {

    public static void main(String[] args) throws Exception {
        var field = new Field(5, 5);
        var communicator = new ConsoleCommunicator();

        var playerList = new ArrayList<Player>();
        playerList.add(new HumanPlayer('X', communicator));
        playerList.add(new AIPlayer('O', new StrategyImpl()));
        var playerController = new PlayerFlowController(playerList);

        while (checkWinner(field) == null) {
            communicator.displayField(field);
            var player = playerController.next();
            var turn = player.play(field);
            var cell = field.getCell(turn.getFirst(), turn.getSecond());
            if (cell.getOwner() != null) {
                throw new Exception("Illegal cell.");
            }
            cell.setOwner(player);
        }

    }

    private static Player checkWinner(Field field) {
        return null;
    }
}
