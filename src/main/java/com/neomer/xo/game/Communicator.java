package com.neomer.xo.game;

import com.neomer.xo.common.Pair;

public interface Communicator {

    void displayField(Field field);

    Pair<Integer, Integer> askForPlay();

}
