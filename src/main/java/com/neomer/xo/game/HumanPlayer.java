package com.neomer.xo.game;

import com.neomer.xo.common.Pair;

public class HumanPlayer implements Player {

    private final Communicator communicator;

    private final char mark;

    public HumanPlayer(char mark, Communicator communicator) {
        this.communicator = communicator;
        this.mark = mark;
    }

    @Override
    public Pair<Integer, Integer> play(final Field field) {
        return communicator.askForPlay();
    }

    @Override
    public char getMark() {
        return mark;
    }
}
