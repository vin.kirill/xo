package com.neomer.xo.game;

import java.util.Iterator;

public class Field {

    private final int width, height;

    private final Cell[][] cells;

    public Field(int width, int height) {
        this.width = width;
        this.height = height;

        cells = new Cell[width][height];
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                cells[x][y] = new Cell(x, y);
            }
        }
    }

    public boolean contains(int x, int y) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Cell getCell(int x, int y) {
        return cells[x][y];
    }

    public ForEachIterator createForEachIterator() {
        return new ForEachIterator(this);
    }

    public static class ForEachIterator implements Iterator<Cell> {
        private final Field field;

        private int currentRow, currentColumn;

        public ForEachIterator(Field field) {
            this.field = field;
            this.currentColumn = this.currentRow = 0;
        }

        @Override
        public boolean hasNext() {
            return currentRow < (field.height - 1)
                    || ((currentRow == field.height - 1) && (this.currentColumn <= field.width - 1));
        }

        @Override
        public Cell next() {
            var result = field.cells[currentColumn][currentRow];
            moveToNext();
            return result;
        }

        private void moveToNext() {
            if (currentColumn == field.width - 1) {
                currentColumn = 0;
                ++currentRow;
            } else {
                ++currentColumn;
            }
        }
    }
}
