package com.neomer.xo.game;

import com.neomer.xo.common.Pair;

public interface Player {

    Pair<Integer, Integer> play(final Field field);

    char getMark();

}
