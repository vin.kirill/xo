package com.neomer.xo.game;

import java.util.List;

public class PlayerFlowController {

    private final List<Player> playerList;

    private int index;

    public PlayerFlowController(List<Player> playerList) {
        this.playerList = playerList;
        index = 0;
    }

    public Player next() {
        if (index == playerList.size()) {
            index = 0;
        }
        return playerList.get(index++);
    }
}
