package com.neomer.xo.game;

public class Cell {

    private Player owner;

    private final int x, y;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
        this.owner = null;
    }


    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
