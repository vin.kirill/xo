package com.neomer.xo.game;

import com.neomer.xo.common.Pair;

import java.util.Scanner;

public class ConsoleCommunicator implements Communicator {

    private final Scanner scanner;

    public ConsoleCommunicator() {
        scanner = new Scanner(System.in);
    }

    @Override
    public void displayField(Field field) {
        Cell prevCell = null;
        for (var it = field.createForEachIterator(); it.hasNext();) {
            var cell = it.next();

            if (prevCell != null && prevCell.getY() != cell.getY()) {
                System.out.print(" |\n | ");
            } else {
                System.out.print(" | ");
            }
            System.out.print(cell.getOwner() != null ? cell.getOwner().getMark() : ' ');

            prevCell = cell;
        }
        System.out.println(" |");
        System.out.println();
    }

    @Override
    public Pair<Integer, Integer> askForPlay() {
        var pair = new Pair<Integer, Integer>(0, 0);
        System.out.print("X = ");
        pair.setFirst(scanner.nextInt());
        System.out.print("Y = ");
        pair.setSecond(scanner.nextInt());
        return pair;
    }
}
